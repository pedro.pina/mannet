/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Calendar;

/**
 *
 * @author Aluno
 */
public class Cliente {
   private String nome;
    private char numero,email;
    private Calendar data;

    public String getNome() {
        return nome;
    }

    public char getNumero() {
        return numero;
    }

    public char getEmail() {
        return email;
    }

    public Calendar getData() {
        return data;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setNumero(char numero) {
        this.numero = numero;
    }

    public void setEmail(char email) {
        this.email = email;
    }

    public void setData(Calendar data) {
        this.data = data;
    }
    
    
    

}
